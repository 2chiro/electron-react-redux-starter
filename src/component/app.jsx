import React, {Component} from 'react'

import {remote} from 'electron'

const BrowserWindow = remote.BrowserWindow
const dialog = remote.dialog

export default class App extends Component {
  openMessage () {
    var options = {
      title: 'メッセージ',
      type: 'info',
      message: 'これはテストダイアログだよ',
    }
    var win = BrowserWindow.getFocusedWindow()
    dialog.showMessageBox(win, options)
  }
  render () {
    return (
      <div className='app'>
        <p>現在の値：{this.props.num}</p>
        <button type='button' onClick={() => this.props.clickHandler()}>1増やす</button><hr />
        <button type='button' onClick={() => this.openMessage()}>開く</button>
      </div>
    )
  }
}

