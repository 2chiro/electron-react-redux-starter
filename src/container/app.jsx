import {connect} from 'react-redux'
import React from 'react'

import App from '../component/app'
import Actions from '../action'

const mapStateToProps = state => {
    return state
}

const mapDispatchToProps = dispatch => {
  return {
    clickHandler: () => {
        dispatch(Actions.increment())
    }
}
}

export default connect(mapStateToProps, mapDispatchToProps)(App)