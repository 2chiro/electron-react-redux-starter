# Electron-React-Redux-Starter

## Install

First, clone the repo the new project folder.

> git clone https://bitbucket.org/2chiro/electron-react-redux-starter.git project-name

And then install the dependencies with npm.

> cd project-name
> npm install

## Usage

### Build the app

> npm run build

### Run the app

> npm start